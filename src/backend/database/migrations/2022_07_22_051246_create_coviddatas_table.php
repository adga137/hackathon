<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoviddatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coviddatas', function (Blueprint $table) {
            $table->id();

            $table->text("trialip")->nullable();
            $table->text("last_refreshed_on")->nullable();
            $table->text("public_title")->nullable();
            $table->text("scientific_title")->nullable();
            $table->text("acronym")->nullable();
            $table->text("primary_sponsor")->nullable();
            $table->text("date_registration")->nullable();
            $table->text("date_registration3")->nullable();
            $table->text("export_date")->nullable();
            $table->text("source_register")->nullable();
            $table->text("web_address")->nullable();
            $table->text("Recruitment Status")->nullable();
            $table->text("other_records")->nullable();
            $table->text("inclusion_agemin")->nullable();
            $table->text("inclusion_agemax")->nullable();
            $table->text("inclusion_gender")->nullable();
            $table->text("date_enrollement")->nullable();
            $table->text("target_size")->nullable();
            $table->text("study_type")->nullable();
            $table->text("study_design")->nullable();
            $table->text("phase")->nullable();
            $table->text("countries")->nullable();
            $table->text("contact_firstname")->nullable();
            $table->text("contact_lastname")->nullable();
            $table->text("contact_address")->nullable();
            $table->text("contact_email")->nullable();
            $table->text("contact_tel")->nullable();
            $table->text("contact_affiliation")->nullable();
            $table->text("inclusion_criteria")->nullable();
            $table->text("exclusion_criteria")->nullable();
            $table->text("condition")->nullable();
            $table->text("intervention")->nullable();
            $table->text("primary_outcome")->nullable();
            $table->text("results_date_posted")->nullable();
            $table->text("results_date_completed")->nullable();
            $table->text("results_url_link")->nullable();
            $table->text("retrospective_flag")->nullable();
            $table->text("bridging_flag_truefalse")->nullable();
            $table->text("bridged_type")->nullable();
            $table->text("results_yes_no")->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coviddatas');
    }
}
