<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\FastExcel;

class DataController extends Controller
{
    //
    public function loadDatabyTCSV($csvfile){
        /*
        $fp = fopen ($csvfile,"r");

        $i = 0;

        while ($dt = fgetcsv ($fp, 1000, "\r")) {
            
            if($i>0){
                
                //$data = explode('|', $dt);

                dd($dt);

                $resp = DB::table("coviddatas")->insert([
                    "trialip" => $data[0],
                    "last_refreshed_on" => $data[1],
                    "public_title" => $data[2],
                    "scientific_title" => $data[3],
                    "acronym" => $data[4],
                    "primary_sponsor" => $data[5],
                    "date_registration" => $data[6],
                    "date_registration3" => $data[7],
                    "export_date" => $data[8],
                    "source_register" => $data[9],
                    "web_address" => $data[10],
                    "Recruitment Status" => $data[11],
                    "other_records" => $data[12],
                    "inclusion_agemin" => $data[13],
                    "inclusion_agemax" => $data[14],
                    "inclusion_gender" => $data[15],
                    "date_enrollement" => $data[16],
                    "target_size" => $data[17],
                    "study_type" => $data[18],
                    "study_design" => $data[19],
                    "phase" => $data[20],
                    "countries" => $data[21],
                    "contact_firstname" => $data[22],
                    "contact_lastname" => $data[23],
                    "contact_address" => $data[24],
                    "contact_email" => $data[25],
                    "contact_tel" => $data[26],
                    "contact_affiliation" => $data[27],
                    "inclusion_criteria" => $data[28],
                    "exclusion_criteria" => $data[29],
                    "condition" => $data[30],
                    "intervention" => $data[31],
                    "primary_outcome" => $data[32],
                    "results_date_posted" => $data[33],
                    "results_date_completed" => $data[34],
                    "results_url_link" => $data[35],
                    "retrospective_flag" => $data[36],
                    "bridging_flag_truefalse" => $data[37],
                    "bridged_type" => $data[38],
                    "results_yes_no" => $data[39]
                ]);

                if(!$resp){
                    return false;
                }
            }

            $i++;
        }
        fclose($fp);
        */

        $coviddatas = (new FastExcel)->import($csvfile, function ($data/*linea de datos*/) {

            return DB::table("coviddatas")->insert([
                "trialip" => $data['TrialID'],
                "last_refreshed_on" => $data['Last Refreshed on'],
                "public_title" => $data['Public title'],
                "scientific_title" => $data['Scientific title'],
                "acronym" => $data['Acronym'],
                "primary_sponsor" => $data['Primary sponsor'],
                "date_registration" => (is_null($data['Date registration']))? "" : (is_numeric($data['Date registration']))? "" : ($data['Date registration'])->format('Y-m-d H:i:s'),
                "date_registration3" => $data['Date registration3'],
                "export_date" => $data['Export date'],
                "source_register" => $data['Source Register'],
                "web_address" => $data['web address'],
                "Recruitment Status" => $data['Recruitment Status'],
                "other_records" => $data['other records'],
                "inclusion_agemin" => $data['Inclusion agemin'],
                "inclusion_agemax" => $data['Inclusion agemax'],
                "inclusion_gender" => $data['Inclusion gender'],
                "date_enrollement" => $data['Date enrollement'],
                "target_size" => strval($data['Target size']),
                "study_type" => $data['Study type'],
                "study_design" => strval($data['Study design']),
                "phase" => $data['Phase'],
                "countries" => $data['Countries'],
                "contact_firstname" => $data['Contact Firstname'],
                "contact_lastname" => $data['Contact Lastname'],
                "contact_address" => $data['Contact Address'],
                "contact_email" => $data['Contact Email'],
                "contact_tel" => $data['Contact Tel'],
                "contact_affiliation" => $data['Contact Affiliation'],
                "inclusion_criteria" => $data['Inclusion Criteria'],
                "exclusion_criteria" => $data['Exclusion Criteria'],
                "condition" => $data['Condition'],
                "intervention" => $data['Intervention'],
                "primary_outcome" => $data['Primary outcome'],
                "results_date_posted" => $data['results date posted'],
                "results_date_completed" => $data['results date completed'],
                "results_url_link" => $data['results url link'],
                "retrospective_flag" => $data['Retrospective flag'],
                "bridging_flag_truefalse" => strval($data['Bridging flag truefalse']),
                "bridged_type" => $data['Bridged type'],
                "results_yes_no" => $data['results yes no']
            ]);
        });

        return true;
    }
}
