<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    //

    public function datasearch($datestart, $dateend){

        $data = DB::table("coviddatas")
                    ->where("date_registration",">=",$datestart)
                    ->andWhere("date_registration","<=",$dateend)
                    ->get();

        return json_decode(["status" => true, "data" => $data]);
    }

    public function countcountriesbytypeStudy(Request $request){

        $data = DB::table("coviddatas")
                    ->where("type_of_study",$request->input("Type of Study"))
                    ->get();

        return json_decode(["status" => true, "data" => $data]);
    }

    public function countstudiesbyintitution(){

        $data = DB::table("coviddatas")
                    ->count();

        return json_decode(["status": true, "data": $data ]);
    }
}
