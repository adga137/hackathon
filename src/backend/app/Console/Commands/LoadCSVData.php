<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\DataController;

class LoadCSVData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:loaddata';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Carga de data sobre el covid desde tsv en storage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->newLine();
        $this->line('+------------------------------------------+');
        $this->line('| Proceso de carga de data de CSV iniciado |');
        $this->line('+------------------------------------------+');
        $this->newLine();

        $this->line('Cargando data de CSV a DB');
        $this->line('Esto puede durar largo tiempo');
        $this->newLine();

        $fpath = storage_path("app/private/datacovid.ods");

        $dc = new DataController();
        $re = $dc->loadDatabyTCSV($fpath);

        if($re){
            $this->info('Carga realizada con exito');
        }else{
            $this->error('Error realizando la carga');
        }

        return 0;
    }
}
